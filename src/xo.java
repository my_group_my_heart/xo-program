
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 66986
 */
public class xo {
  static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner sc = new Scanner(System.in);
    static boolean finish = false;
    
    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable(table);
            showTurn(currentPlayer);
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }

    }

    public static void showTable(char[][] _table) {
        for (int r = 0; r < _table.length; r++) {
            for (int c = 0; c < _table[r].length; c++) {
                System.out.print(_table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }
    
    public static void showTurn(char current) {
        System.out.println("Turn " + current);
    }

    public static void inputRowCol() {
        System.out.println("Please input row,Col");
        row = sc.nextInt();
        col = sc.nextInt();
    }
    
    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    
    public static boolean checkVertical(char[][] _table, char currentPlayer, int col) {
        for (int row = 0; row < _table.length; row++) {
            if (_table[row][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal(char[][] _table, char currentPlayer, int row) {
        for (int col = 0; col < _table.length; col++) {
            if (_table[row - 1][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }
    public static boolean checkX(char[][] _table, char _currentPlayer) {
        if (checkX1(_table, _currentPlayer)) {
            return true;
        } else if (checkX2(_table, _currentPlayer)) {
            return true;
        }
        return false;

    }

    private static boolean checkX1(char[][] _table, char _currentPlayer) {
        for (int i = 0; i < _table.length; i++) {
            if (_table[i][i] != _currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2(char[][] _table, char _currentPlayer) {
        for (int i = 0; i < _table.length; i++) {
            if (_table[i][2 - i] !=_currentPlayer) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean checkDraw(char[][] _table) {
        for (int i = 0; i < _table.length; i++) {
            for (int j = 0; j < _table.length; j++) {
                if (_table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    public static void process() {
        if (setTable()) {
            if (checkWin(table, currentPlayer, row, col)) {
                finish = true;
                showWin(table);
                return;
            }
            switchPlayer();
        }

    }
    private static boolean checkWin(char[][] _table, char currentPlayer, int row, int col) {
        if (checkVertical(_table, currentPlayer, col)) {
            return true;
        } else if (checkHorizontal(_table, currentPlayer, row)) {
            return true;
        } else if (checkX(_table, currentPlayer)) {
            return true;
        } else if (checkDraw(_table)) {
            return true;
        }
        return false;

    }
    private static void showWin(char[][] _table) {
        showTable(_table);
        if (checkDraw(_table)) {
            System.out.println(">>>Draw<<<");
            return;
        } else {
            System.out.println(">>>" + currentPlayer + " Win<<<");
        }

    }
}
